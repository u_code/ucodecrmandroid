package com.ucode.crm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

/**
 * @author Kevin
 * @data 2019-06-13
 */
public class CustomDebugTree extends Timber.DebugTree {
    @Override
    protected @Nullable String createStackElementTag(@NotNull StackTraceElement element) {
        return String.format("[C:%s] [%s (%s:%s)]",
                super.createStackElementTag(element),
                element.getMethodName(),
                element.getFileName(),
                element.getLineNumber()
        );
    }



    @Override
    protected void log(int priority, String tag, @NotNull String message, Throwable t) {
        if (tag != null) {
            String threadName = Thread.currentThread().getName();
            tag = tag + String.format(" [T:%s] ", threadName);
        }
        super.log(priority, tag, message, t);
    }
}
