package com.ucode.crm

import com.ucode.crm.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import org.lovedev.util.Utils
import timber.log.Timber

/**
 * @author Kevin
 * @date 2018/8/9
 */
class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(CustomDebugTree())
        Utils.init(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}