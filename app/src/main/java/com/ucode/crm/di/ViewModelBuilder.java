package com.ucode.crm.di;

import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;


/**
 * @author Kevin
 * @data 2018/8/28
 */
@Module
abstract class ViewModelBuilder {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}