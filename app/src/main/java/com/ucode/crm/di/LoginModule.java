package com.ucode.crm.di;

/**
 * @author Kevin
 * @date 2020-02-18
 */

import com.ucode.crm.ui.login.LoginActivity;
import com.ucode.crm.ui.login.LoginViewModel;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
abstract class LoginModule {
    @ContributesAndroidInjector(modules = ViewModelBuilder.class)
    abstract LoginActivity bindLoginActivity();

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindsLoginViewModel(LoginViewModel viewModel);
}
