package com.ucode.crm.di;

import android.app.Application;
import android.content.Context;

import com.ucode.crm.App;

import dagger.Binds;
import dagger.Module;

/**
 * @author Kevin
 * @date 2018/8/27
 */
@Module
abstract class AppModule {

    @Binds
    abstract Context bindContext(App application);

    @Binds
    abstract Application bindApplication(App application);
}