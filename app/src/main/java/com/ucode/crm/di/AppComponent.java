package com.ucode.crm.di;

import com.ucode.crm.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * @author Kevin
 * @date 2018/8/27
 */
@Singleton
@Component(modules = {
        AppModule.class,
        ApiModule.class,
        LoginModule.class,
        AndroidSupportInjectionModule.class
})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Factory
    interface Factory {
        AppComponent create(@BindsInstance App application);
    }
}