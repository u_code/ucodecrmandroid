package com.ucode.crm.base;

import android.os.Bundle;

import javax.inject.Inject;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * @author Kevin
 * @date 2020-02-18
 */
public abstract class BaseActivity <VM extends ViewModel, DB extends ViewDataBinding> extends DaggerAppCompatActivity {
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    protected androidx.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent();


    public VM viewModel;

    public DB dataBinding;

    public abstract Class<VM> getViewModel();

    @LayoutRes
    public abstract int getLayoutRes();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());

        dataBinding = DataBindingUtil.setContentView(this, getLayoutRes(), dataBindingComponent);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel());
    }
}
