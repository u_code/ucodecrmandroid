package com.ucode.crm.ui.login;

import android.os.Bundle;

import com.ucode.crm.App;
import com.ucode.crm.R;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;
import timber.log.Timber;

/**
 * @author Kevin
 */
public class LoginActivity extends DaggerAppCompatActivity {

    @Inject App mApp;
    @Inject ViewModelProvider.Factory mViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Timber.i(mApp.toString());

        viewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel.class);
    }
}
